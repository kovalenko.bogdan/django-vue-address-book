# Contacts

## Description

- This is test task for CHI Software [app demo](http://134.209.227.244/)
- This project template was generated with [cookiecutter](https://cookiecutter-django.readthedocs.io/en/latest/)

## Requirements for "in docker" setup
- node `8.11.4` or higher
- npm `5.6.0` or higher
- docker and docker-compose

## Development installation
 - `git clone git@gitlab.com:kovalenko.bogdan/django-vue-address-book.git`
 - `cd django-vue-address-book`
 - `python3 -m venv venv`
 - `source venv/bin/activate`
 - `pip install -r requirements/local.txt`
 - `npm install`
 - `export USE_DOCKER=False`
 - `npm run dev` - will start python development server and js/sass watch
 - `python manage.py migrate`


## Docker development server
 - `git clone git@gitlab.com:kovalenko.bogdan/django-vue-address-book.git`
 - `cd django-vue-address-book`
 - `npm install`
 - `npm run build`
 - `docker-compose -f local.yml build`
 - `docker-compose -f local.yml up`



