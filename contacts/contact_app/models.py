from django.db import models
from django.db.models import CharField, ImageField, URLField, ForeignKey


class ContactsModel(models.Model):
    name = CharField(max_length=255)
    surname = CharField(max_length=255)
    country = CharField(max_length=255, blank=True)
    city = CharField(max_length=255, blank=True)
    address = CharField(max_length=255, blank=True)
    tags = CharField(max_length=255, blank=True)

    class Meta:
        unique_together = ('name', 'surname',)

    def __str__(self):
        return "%s %s" % (self.name, self.surname,)


class TelephoneNumber(models.Model):
    contact = ForeignKey(ContactsModel, on_delete=models.CASCADE, related_name='telephones')
    telephone = CharField(max_length=255)

    def __str__(self):
        return self.telephone


class Images(models.Model):
    contact = ForeignKey(ContactsModel, on_delete=models.CASCADE, related_name='images')
    image = ImageField(blank=True, default=None)

    def __str__(self):
        return self.image.url


class Urls(models.Model):
    contact = ForeignKey(ContactsModel, on_delete=models.CASCADE, related_name='urls')
    url = URLField(blank=True)

    def __str__(self):
        return self.url
