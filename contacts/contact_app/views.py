from rest_framework import viewsets
from contacts.contact_app.serializers import TelephoneSerializer, ImagesSerializer, \
    UrlSerializer, ContactModelSerializer
from contacts.contact_app.models import TelephoneNumber, Images, Urls,  ContactsModel
from rest_framework import filters


class TelephoneViewSet(viewsets.ModelViewSet):
    queryset = TelephoneNumber.objects.all()
    serializer_class = TelephoneSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Images.objects.all()
    serializer_class = ImagesSerializer


class UrlViewSet(viewsets.ModelViewSet):
    queryset = Urls.objects.all()
    serializer_class = UrlSerializer


class ContactsViewSet(viewsets.ModelViewSet):
    queryset = ContactsModel.objects.all()
    serializer_class = ContactModelSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'surname', 'country', 'city', 'address', 'tags', 'telephones__telephone', 'urls__url',)
