from django.apps import AppConfig


class ContactAppConfig(AppConfig):
    name = 'contacts.contact_app'
