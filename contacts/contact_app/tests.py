from django.test import TestCase
from contacts.contact_app.models import ContactsModel, TelephoneNumber, Urls, Images
import django
from django.test import Client
import json
from rest_framework import status
from django.utils.six import BytesIO
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import SimpleUploadedFile
from PIL import Image


class ModelTestCase(TestCase):
    """Testing contact_app model"""

    def setUp(self):
        ContactsModel.objects.create(name="Name", surname="Surname")

    def test_object_created(self):
        contact = ContactsModel.objects.get(name="Name")
        self.assertTrue(isinstance(contact, ContactsModel))

    def test_unique_name_and_surname(self):
        with self.assertRaises(django.db.utils.IntegrityError):
            ContactsModel.objects.create(name="Name", surname="Surname")

    def test_telephone_object_creation(self):
        contact = ContactsModel.objects.get(name="Name")
        tel = TelephoneNumber.objects.create(telephone="9379992", contact=contact)
        self.assertTrue(isinstance(tel, TelephoneNumber))

    def test_url_object_creation(self):
        contact = ContactsModel.objects.get(name="Name")
        url = Urls.objects.create(url="https://vk.com", contact=contact)
        self.assertTrue(isinstance(url, Urls))

    def test_images_object_creation(self):
        contact = ContactsModel.objects.get(name="Name")
        image = Images.objects.create(contact=contact, image='')
        self.assertEqual(image.contact, contact)


class PagesTestCase(TestCase):
    """Testing home page loading"""
    def setUp(self):
        self.c = Client()

    def test_home_page_render(self):
        response = self.c.get('/')
        self.assertContains(response, "contacts")
        self.assertContains(response, "Home")


class RestApiCase(TestCase):
    """Test api endpoints and CRUD operations"""
    def setUp(self):
        self.c = Client()
        self.telephone_pk = None
        self.contact_pk = None
        self.url_pk = None
        self.image_pk = None
        contact = ContactsModel.objects.create(name="Name", surname="Surname")
        TelephoneNumber.objects.create(telephone="9379992", contact=contact)
        Urls.objects.create(url="https://vk.com", contact=contact)
        Images.objects.create(contact=contact, image='')

    def create_image(self, storage, filename, size=(100, 100,), image_mode='RGB', image_format='PNG'):
        """
        Generate a test image, returning the filename that it was saved as.
        """
        data = BytesIO()
        Image.new(image_mode, size).save(data, image_format)
        data.seek(0)
        if not storage:
            return data
        image_file = ContentFile(data.read())
        return storage.save(filename, image_file)

    def test_api_root(self):
        response = self.c.get('/api/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'telephones')
        self.assertContains(response, 'urls')
        self.assertContains(response, 'contacts')
        self.assertContains(response, 'images')

    def test_urls_endpoint(self):
        response = self.c.get('/api/urls/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "https://vk.com")

    def test_telephones_endpoint(self):
        response = self.c.get('/api/telephones/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "9379992")

    def test_images_endpoint(self):
        response = self.c.get('/api/images/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "pk")
        self.assertContains(response, "contact")

    def test_contacts_endpoint(self):
        response = self.c.get('/api/contacts/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "Name")
        self.assertContains(response, "Surname")
        self.assertContains(response, "9379992")
        self.assertContains(response, "https://vk.com")

    def test_invalid_contacts_object_creation(self):
        obj = {'username': 'john', 'password': 'smith'}
        response = self.c.post('/api/contacts/', obj)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_contacts_object_creation(self):
        valid_object = {
            "name": "Bogdan",
            "surname": "Kovalenko",
            "country": "UA",
            "city": "Zp",
            "address": "ndpr, 14",
            "tags": "Hello, World",
            "telephones": [
                {
                    "telephone": "8800880088"
                }
            ],
            "urls": [
                {
                    "url": "https://fb.com"
                }
            ],
            "images": []
        }
        response = self.c.post('/api/contacts/', data=json.dumps(valid_object),
                               content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.contact_pk = json.loads(response.content.decode('utf-8'))['pk']
        contact = ContactsModel.objects.get(name="Bogdan")
        self.assertEqual(contact.surname, "Kovalenko")

    def test_contacts_object_update(self):
        self.test_contacts_object_creation()
        updated_object = {
            "name": "Eugen",
            "surname": "Filonenko",
            "country": "UA",
            "city": "Zp",
            "address": "ndpr, 14",
            "tags": "Hello, World",
            "telephones": [
                {
                    "telephone": "8800880088"
                }
            ],
            "urls": [

            ],
            "images": []
        }
        response = self.c.put('/api/contacts/%s/' % self.contact_pk,
                              data=json.dumps(updated_object),
                              content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "Filonenko")

        response = self.c.get('/api/contacts/%s/' % self.contact_pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "Filonenko")

    def test_telephone_creation(self):
        telephone = {
            'telephone': '1111',
            'contact': '1'
        }
        response = self.c.post('/api/telephones/', data=json.dumps(telephone),
                               content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.telephone_pk = json.loads(response.content.decode('utf-8'))['pk']
        telephone_object = TelephoneNumber.objects.get(telephone="1111")
        self.assertTrue(isinstance(telephone_object, TelephoneNumber))

    def test_telephone_update(self):
        self.test_telephone_creation()
        telephone = {
            'telephone': '2222',
            'contact': '1'
        }
        response = self.c.put('/api/telephones/%s/' % self.telephone_pk,
                              data=json.dumps(telephone),
                              content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "2222")

        response = self.c.get('/api/telephones/%s/' % self.telephone_pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "2222")

    def test_telephone_deletion(self):
        self.test_telephone_creation()
        response = self.c.delete('/api/telephones/%s/' % self.telephone_pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_url_creation(self):
        url = {
            'url': 'https://github.com',
            'contact': '1'
        }
        response = self.c.post('/api/urls/', data=json.dumps(url),
                               content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.url_pk = json.loads(response.content.decode('utf-8'))['pk']
        url_object = Urls.objects.get(url="https://github.com")
        self.assertTrue(isinstance(url_object, Urls))

    def test_url_update(self):
        self.test_url_creation()
        url = {
            'url': 'https://gitlab.com',
            'contact': '1'
        }
        response = self.c.put('/api/urls/%s/' % self.url_pk,
                              data=json.dumps(url),
                              content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "https://gitlab.com")

        response = self.c.get('/api/urls/%s/' % self.url_pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "https://gitlab.com")

    def test_url_deletion(self):
        self.test_url_creation()
        response = self.c.delete('/api/urls/%s/' % self.url_pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_image_creation(self):
        self.test_contacts_object_creation()
        avatar = self.create_image(None, 'avatar.png')
        avatar_file = SimpleUploadedFile('front.png', avatar.getvalue())
        form_data = {'image': avatar_file, 'contact': self.contact_pk}
        response = self.c.post('/api/images/', form_data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.image_pk = response.data['pk']

    def test_image_deletion(self):
        self.test_image_creation()
        response = self.c.delete('/api/images/%s/' % self.image_pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_search_engine(self):
        self.test_telephone_creation()
        response = self.c.get('/api/contacts?search=1111', follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "1111")

        response = self.c.get('/api/contacts?search=2222', follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, "2222")
