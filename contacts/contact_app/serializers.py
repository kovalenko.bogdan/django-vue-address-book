from contacts.contact_app.models import ContactsModel, TelephoneNumber, Images, Urls
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator


class TelephoneSerializer(serializers.ModelSerializer):
    """Telephone model serializer"""
    class Meta:
        model = TelephoneNumber
        fields = ('pk', 'telephone', 'contact')


class TelephoneNestedSerializer(serializers.ModelSerializer):
    """Telephone serializer for using inside ContactModelSerializer"""
    class Meta:
        model = TelephoneNumber
        fields = ('pk', 'telephone')


class ImagesSerializer(serializers.ModelSerializer):
    """Image model serializer"""
    class Meta:
        model = Images
        fields = ('pk', 'image', 'contact')


class UrlSerializer(serializers.ModelSerializer):
    """Url model serializer"""
    class Meta:
        model = Urls
        fields = ('pk', 'url', 'contact')


class UrlNestedSerializer(serializers.ModelSerializer):
    """Url serializer for using inside ContactModelSerializer"""
    class Meta:
        model = Urls
        fields = ('pk', 'url')


class ContactModelSerializer(serializers.ModelSerializer):
    """Contacts model serializer"""
    city = serializers.CharField(allow_blank=True)
    country = serializers.CharField(allow_blank=True)
    address = serializers.CharField(allow_blank=True)
    telephones = TelephoneNestedSerializer(many=True)
    images = ImagesSerializer(many=True, required=False, read_only=True)
    urls = UrlNestedSerializer(many=True, allow_null=True)

    def validate_telephones(self, attrs):
        """Custom validator for telephones field.
            Checks if at least one telephone number passed on creation"""
        if len(attrs) == 0:
            raise serializers.ValidationError('at least one offer required')
        return attrs

    def update(self, instance, validated_data):
        """Updates Contacts Model. Ignores changes in nested fields"""
        instance.name = validated_data.get('name', instance.name)
        instance.surname = validated_data.get('surname', instance.surname)
        instance.country = validated_data.get('country', instance.country)
        instance.city = validated_data.get('city', instance.city)
        instance.address = validated_data.get('address', instance.address)
        instance.tags = validated_data.get('tags', instance.tags)
        instance.save()
        return instance

    def create(self, validated_data):
        """Creates Contact Model with nested objects, except images"""
        telephones = validated_data.pop('telephones')
        urls = validated_data.pop('urls')
        contact = ContactsModel.objects.create(**validated_data)
        for telephone_data in telephones:
            TelephoneNumber.objects.get_or_create(contact=contact, **telephone_data)
        for url_data in urls:
            Urls.objects.get_or_create(contact=contact, **url_data)
        return contact

    class Meta:
        model = ContactsModel
        fields = ('pk', 'name', 'surname', 'country', 'city', 'address',
                  'tags', 'telephones', 'urls', 'images')
        # Validation for unique name and username combination
        validators = [
            UniqueTogetherValidator(
                queryset=ContactsModel.objects.all(),
                fields=('name', 'surname')
            )
        ]
