from django.contrib import admin
from contacts.contact_app.models import TelephoneNumber, ContactsModel, Urls, Images

# Register your models here.
admin.site.register(TelephoneNumber)
admin.site.register(ContactsModel)
admin.site.register(Urls)
admin.site.register(Images)
