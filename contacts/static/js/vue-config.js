
Vue.http.headers.common['X-CSRFToken'] = "{{ csrf_token }}";
new Vue({
	el: '#app',
	delimiters: ['${', '}'],
	data: {
		contacts: [],
		currentContact: {},
		message: null,
		newContact: {
			'name': null,
			'surname': null,
			'country': '',
			'city': '',
			'address': '',
			'tags': '',
			'telephones': [{
				'telephone': ''
			}],
			'urls': [],
		},
		search_term: '',
		images: [],
		urls: {},
		telephone: {},
		currentImage: {},
	},
	mounted: function () {
		this.getContacts();
	},
	methods: {
		onFileChange(e, index) {
		this.images[index].image = e.target.files[0]
    	},
		onEditFileChange(e) {
		this.currentImage.image = e.target.files[0]
    	},
		addImage: function (id) {
			// Go through images array and create images object (upload)
			for (let item in this.images) {
				if (!this.images[item].image) continue;
				let fd = new FormData();
				fd.append("image", this.images[item].image, this.images[item].image.name);
				fd.append('contact', id);

				this.$http.post('/api/images/', fd).then((response) => {
				console.log(response.data);
				this.getContacts();
				this.getContact(this.currentContact.pk);
				$('#addImageModal').modal('hide');
				});
			};
			this.images = [];
		},
		updateImage: function (id) {
			// Go through images array and create images object (upload)
				if (!this.currentImage.image) return;
				let fd = new FormData();
				fd.append("image", this.currentImage.image, this.currentImage.image.name);
				fd.append('contact', id);


				this.$http.put(`/api/images/${this.currentImage.pk}/`, fd).then((response) => {
				console.log(response.data);
				this.getContacts();
				this.getContact(this.currentContact.pk);
				$('#editImageModal').modal('hide');
				});
			this.currentImage = {};
		},
		showImageModal: function () {
			$('#addImageModal').modal('show');
			this.images = [{'image': null, 'contact': null}];
		},
		showUrlModal: function () {
			$('#addUrlModal').modal('show');
			this.urls = {'url': null, 'contact': null};
		},
		submitUrl: function(pk) {
			this.addUrl(pk);
			this.getContacts();
			this.getContact(pk);
			$('#addUrlModal').modal('hide');
		},
		addUrl: function (pk) {
			this.urls.contact = pk;
				this.$http.post('/api/urls/', this.urls)
					.then((response) => {
						this.getContacts();
						this.getContact(pk);
						this.urls = {};
						$("#addUrlModal").modal('hide');
					})
					.catch((err) => {
						console.log(err);
					});
		},
		showTelephoneModal: function () {
			$('#addTelephoneModal').modal('show');
			this.telephone = {'telephone': null, 'contact': null};
		},
		submitTelephone: function(pk) {
			this.addTelephone(pk);
			this.getContacts();
			this.getContact(pk);
			$('#addTelephoneModal').modal('hide');
		},
		addTelephone: function (pk) {
			this.telephone.contact = pk;
				this.$http.post('/api/telephones/', this.telephone)
					.then((response) => {
						this.getContacts();
						this.getContact(pk);
						this.telephone = {};
						$("#addTelephoneModal").modal('hide');
					})
					.catch((err) => {
						console.log(err);
					});
		},
		showEditImageModal: function (i) {
			$('#editImageModal').modal('show');
			this.currentImage = this.currentContact.images[i];
		},
		getContacts: function (clear = false) {
			if (clear === true ) {
				this.search_term = '';
			}
			let api_url = 'api/contacts/';
			if (this.search_term !== '' || this.search_term !== null) {
				api_url = `/api/contacts/?search=${this.search_term}`;
			}
			this.$http.get(api_url)
				.then((response) => {
					this.contacts = response.data;
				})
				.catch((err) => {
					console.log(err);
				});
		},
		getContact: function (pk) {
			this.$http.get(`/api/contacts/${pk}/`)
				.then((response) => {
					this.currentContact = response.data;
					$("#editContactModal").modal('show');

				})
				.catch((err) => {
					console.log(err);
				});
		},
		addContact: function () {
				this.$http.post('/api/contacts/', this.newContact)
					.then((response) => {
						this.addImage(response.data.pk);
						this.getContacts();
						$("#addContactModal").modal('hide');
						this.newContact.name = '';
						this.newContact.surname = '';
						this.newContact.country = '';
						this.newContact.city = '';
						this.newContact.address = '';
						this.newContact.tags = '';
						this.newContact.urls = [];
						this.newContact.telephones = [{
							'telephone': ''
						}];
					})
					.catch((err) => {
						console.log(err);
					});
		},
		deleteImage: function (pk) {
			this.$http.delete(`/api/images/${pk}/`)
				.then((response) => {
					this.getContacts();
					this.getContact(this.currentContact.pk);
				})
				.catch((err) => {
					console.log(err);
				});
		},
		deleteTelephone: function (pk) {
			this.$http.delete(`/api/telephones/${pk}/`)
				.then((response) => {
					this.getContacts();
					this.getContact(this.currentContact.pk);
				})
				.catch((err) => {
					console.log(err);
				});
		},
		deleteUrl: function (pk) {
			this.$http.delete(`/api/urls/${pk}/`)
				.then((response) => {
					this.getContacts();
					this.getContact(this.currentContact.pk);
				})
				.catch((err) => {
					console.log(err);
				});
		},
		editTelephone: function (index) {
			let item = this.currentContact.telephones[index];
			item.contact = this.currentContact.pk;
			let pk = item.pk;
			this.$http.put(`/api/telephones/${pk}/`, item)
				.then((response) => {
					this.currentContact.telephones[index] = response.data;
					this.getContacts();
				})
				.catch((err) => {
					console.log(err);
				});
		},
		editUrl: function (index) {
			let item = this.currentContact.urls[index];
			item.contact = this.currentContact.pk;
			let pk = item.pk;
			this.$http.put(`/api/urls/${pk}/`, item)
				.then((response) => {
					this.currentContact.urls[index] = response.data;
					this.getContacts();
				})
				.catch((err) => {
					console.log(err);
				});
		},
		updateContact: function () {
			this.$http.put(`/api/contacts/${this.currentContact.pk}/`, this.currentContact)
				.then((response) => {
					this.currentContact = response.data;
					this.getContacts();
				})
				.catch((err) => {
					console.log(err);
				});
		},
		deleteContact: function (pk) {
			this.$http.delete(`/api/contacts/${pk}/`)
				.then((response) => {
					this.getContacts();
					$("#editContactModal").modal('hide');
				})
				.catch((err) => {
					console.log(err);
				});
		},
	}
});
